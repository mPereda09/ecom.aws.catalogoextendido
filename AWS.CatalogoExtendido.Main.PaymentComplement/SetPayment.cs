using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Dynamic;
using Newtonsoft.Json;

using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;

using ECOM.AWS.CatalogoExtendido.Business;
using AWS.CatalogoExtendido.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AWS.CatalogoExtendido.Main.PaymentComplement
{
    public class SetPayment
    {
        public APIGatewayProxyResponse FuncSetPayCompHan(APIGatewayProxyRequest request, ILambdaContext context)
        {
            object response = null;
            var  flagLogger = Convert.ToBoolean(request.StageVariables["FlgLogger"]);

            try
            {
                if (flagLogger)
                {
                    LambdaLogger.Log("-----------------------------------StartLog-----------------------------------------------------------");
                    LambdaLogger.Log("LogRequest |" + "| INFO |" + "REQUEST :" + request.Body);
                }

                Dictionary<string, string> stageVariables = request.StageVariables != null ? new Dictionary<string, string>(request.StageVariables, StringComparer.OrdinalIgnoreCase) : null;

                StaticVariables.SetStageVariables(stageVariables);

                response = BizPaymentComplement.SetPaymentComplement(request.Body);

            }
            catch (Exception ex)
            {
                dynamic error = new ExpandoObject();

                error.Code = 636873088929753245;
                error.Message = "Ocurrio un error al ejecutar el complemento de pago.";
                error.Result = null;
                response = JsonConvert.SerializeObject(error);
                if (flagLogger)
                {
                    string stringLog = "{\"Code\":636873088449233257,\"Message\":\"" + "Ocurrio un error al ejecutar el complemento de pago." + " More Info :" + ex.Message.ToString() + " DetailInfo :" + ex.StackTrace.ToString() + "\",\"Result\":null}";
                    LambdaLogger.Log("LogErrorException |" + "| INFO |" + "RESPONSE :" + stringLog);
                }
            }

            if (flagLogger)
            {
                LambdaLogger.Log("LogProcess |" + "| INFO |" + "RESPONSE :" + response);
                LambdaLogger.Log("-------------------------------------EndLog--------------------------------------------------------------");
            }

            return new APIGatewayProxyResponse()
            {
                Body = response.ToString(),
                IsBase64Encoded = false,
                StatusCode = (int)HttpStatusCode.OK,
            };


        }
    }
}
