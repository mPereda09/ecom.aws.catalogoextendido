﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class ComplementPaymentCe
    {
        [JsonProperty("info")]
        public Info Info { get; set; }

        [JsonProperty("paymentcomplement")]
        public Paymentcomplement Paymentcomplement { get; set; }
    }

    public class ComplementPaymentCeObject
    {

        [JsonProperty("paymentcomplement")]
        public Paymentcomplement Paymentcomplement { get; set; }


        [JsonProperty("presupuesto")]
        public List<Info> presupuesto { get; set; }
    }


    public class Info
    {
        [JsonProperty("estacionTrabajo")]
        public string EstacionTrabajo { get; set; }

        [JsonProperty("idUsuario")]

        public string IdUsuario { get; set; }

        [JsonProperty("informacionCliente")]
        public InformacionCliente InformacionCliente { get; set; }

        [JsonProperty("informacionVenta")]
        public List<InformacionVenta> InformacionVenta { get; set; }

        [JsonProperty("referenciaEktCom")]

        public int ReferenciaEktCom { get; set; }

        [JsonProperty("tipoVenta")]
        public int TipoVenta { get; set; }

        [JsonProperty("totalVenta")]
        public decimal TotalVenta { get; set; }

        [JsonProperty("informacionPago")]
        public InformacionPago InformacionPago { get; set; }

        [JsonProperty("consecutivoPedido")]
        public int ConsecutivoPedido { get; set; }

        [JsonProperty("costoEstimado")]
        public decimal CostoEstimado { get; set; }

        [JsonProperty("tipoEntrega")]
        public int TipoEntrega { get; set; }

        [JsonProperty("proveedorEnvio")]
        public string ProveedorEnvio { get; set; }

        [JsonProperty("omnicanalId")]
        public int OmnicanalId { get; set; }

        [JsonProperty("totalPedidos")]
        public int TotalPedidos { get; set; }

        [JsonProperty("afectaConta")]
        public int AfectaConta { get; set; }

        [JsonProperty("marketPlaceId")]
        public int MarketPlaceId { get; set; }

        [JsonProperty("sellerId")]
        public int SellerId { get; set; }

        [JsonProperty("montoVentaMP")]
        public decimal MontoVentaMp { get; set; }

        [JsonProperty("montoComision")]
        public decimal MontoComision { get; set; }

        [JsonProperty("penalizacionId")]
        public int PenalizacionId { get; set; }

        [JsonProperty("montoPenalizacion")]
        public decimal MontoPenalizacion { get; set; }

        [JsonProperty("origenEnvioId")]
        public int OrigenEnvioId { get; set; }

        [JsonProperty("pedidoOtorgaRegalo")]
        public int PedidoOtorgaRegalo { get; set; }

        [JsonProperty("skuOtorgaRegalo")]
        public int SkuOtorgaRegalo { get; set; }

        [JsonProperty("tiendaSurt")]
        public int TiendaSurt { get; set; }
    }

    public class InformacionCliente
    {
        [JsonProperty("ApellidoMaternoCliente")]
        public string ApellidoMaternoCliente { get; set; }

        [JsonProperty("ApellidoPaternoCliente")]
        public string ApellidoPaternoCliente { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("NombreCliente")]
        public string NombreCliente { get; set; }

        [JsonProperty("NombreCompletoCliente")]
        public string NombreCompletoCliente { get; set; }

        [JsonProperty("domicilio")]
        public Domicilio Domicilio { get; set; }
    }

    public class Domicilio
    {
        [JsonProperty("calle")]
        public string Calle { get; set; }

        [JsonProperty("codigoPostal")]
        public string CodigoPostal { get; set; }

        [JsonProperty("colonia")]
        public string Colonia { get; set; }

        [JsonProperty("estado")]
        public string Estado { get; set; }

        [JsonProperty("numeroExterno")]

        public string NumeroExterno { get; set; }

        [JsonProperty("numeroInterno")]
        public string NumeroInterno { get; set; }

        [JsonProperty("poblacion")]
        public string Poblacion { get; set; }
    }

    public class InformacionPago
    {
        [JsonProperty("tipoPago")]
        public int TipoPago { get; set; }

        [JsonProperty("plazoMsi")]
        public int PlazoMsi { get; set; }

        [JsonProperty("bancoId")]

        public int BancoId { get; set; }

        [JsonProperty("numeroTarjeta")]
        public string NumeroTarjeta { get; set; }

        [JsonProperty("referenciaPago")]

        public long ReferenciaPago { get; set; }

        [JsonProperty("monto")]
        public decimal Monto { get; set; }
    }

    public class InformacionVenta
    {
        [JsonProperty("cantidad")]
        public int Cantidad { get; set; }

        [JsonProperty("costoProducto")]
        public decimal CostoProducto { get; set; }

        [JsonProperty("descuento")]
        public decimal Descuento { get; set; }

        [JsonProperty("lstMilenias")]
        public List<LstMilenia> LstMilenias { get; set; }

        [JsonProperty("lstPromociones")]
        public List<LstPromocione> LstPromociones { get; set; }

        [JsonProperty("mecanica")]
        public int Mecanica { get; set; }

        [JsonProperty("precio")]
        public decimal Precio { get; set; }

        [JsonProperty("sku")]
        public int Sku { get; set; }

        [JsonProperty("sobreprecio")]
        public decimal Sobreprecio { get; set; }

        [JsonProperty("descuentoEspecial")]
        public decimal DescuentoEspecial { get; set; }
    }

    public class LstMilenia
    {
        [JsonProperty("sku")]
        public int Sku { get; set; }

        [JsonProperty("sobreprecio")]
        public decimal Sobreprecio { get; set; }
    }

    public class LstPromocione
    {
        [JsonProperty("cantidad")]
        public int Cantidad { get; set; }

        [JsonProperty("idPromocion")]
        public int IdPromocion { get; set; }

        [JsonProperty("idRegalo")]
        public int IdRegalo { get; set; }

        [JsonProperty("monto")]
        public decimal Monto { get; set; }

        [JsonProperty("subTipoPromocion")]
        public int SubTipoPromocion { get; set; }

        [JsonProperty("tipoPromocion")]
        public int TipoPromocion { get; set; }
    }

    public class Paymentcomplement
    {
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("authorizationId")]
        public string AuthorizationId { get; set; }

        [JsonProperty("paymentType")]

        public decimal PaymentType { get; set; }

        [JsonProperty("reference")]
        public string Reference { get; set; }

        [JsonProperty("paymentDate")]
        public DateTime PaymentDate { get; set; }

        [JsonProperty("employeeId")]
        public string EmployeeId { get; set; }
    }
}
