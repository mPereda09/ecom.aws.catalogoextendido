using System;
using System.Collections.Generic;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using ECOM.AWS.CatalogoExtendido.Business;
using ECOM.AWS.CatalogoExtendido.Entities;
using Newtonsoft.Json;
using System.Net;
using AWS.CatalogoExtendido.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace ECOM.AWS.CatalogoExtendido.Main.Search
{
    public class FunctionAutoSuggest
    {
        public APIGatewayProxyResponse FunctionAutoSuggestHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            var ListAutoSuggest = new C3MethodResponse<List<AutoSuggestResponse>>();
            var response = new APIGatewayProxyResponse();
            var loggerLambda = new LoggerLambda();
            try
            {
                Dictionary<string, string> pathParams = request.PathParameters != null ? new Dictionary<string, string>(request.PathParameters, StringComparer.OrdinalIgnoreCase) : null;
                Dictionary<string, string> queryParams = request.QueryStringParameters != null ? new Dictionary<string, string>(request.QueryStringParameters, StringComparer.OrdinalIgnoreCase) : null;
                Dictionary<string, string> stageVariables = request.StageVariables != null ? new Dictionary<string, string>(request.StageVariables, StringComparer.OrdinalIgnoreCase) : null;
                StaticVariables.SetStageVariables(stageVariables);

                loggerLambda.ToWriterLoggs("-----------------------------------STARTLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                loggerLambda.ToWriterLoggs("REQUEST || " + "INFO ||" + JsonConvert.SerializeObject(request.Body), StaticVariables.FlgLogger);

                BizProductSearch productSearchBz = new BizProductSearch();
                ListAutoSuggest = productSearchBz.AutoSuggestProduct(pathParams, queryParams);
                response.StatusCode = (int)HttpStatusCode.OK;
                response.IsBase64Encoded = true;
                response.Body = JsonConvert.SerializeObject(ListAutoSuggest);

                if (Convert.ToBoolean(request.StageVariables["FlgLogger"]))
                {
                    loggerLambda.ToWriterLoggs("RESPONSE || " + "INFO ||", StaticVariables.FlgLogger);

                    if (ListAutoSuggest.result.Count > 0)
                    {
                        LambdaLogger.Log("---------<LIST-SKUS-RESPONSE>--------------------");

                        foreach (var item in ListAutoSuggest.result)
                        {
                            loggerLambda.ToWriterLoggs(Convert.ToString(item.productId), request.StageVariables["FlgLogger"]);
                        }
                        LambdaLogger.Log("---------<LIST-SKUS-RESPONSE>--------------------");
                    }
                }

            }
            catch (Exception ex)
            {
                loggerLambda.ToWriterLoggs("-----------------------------------EXCEPTIONLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                loggerLambda.ToWriterLoggs("ERROR-EXCEPTION || " + "INFO ||" + JsonConvert.SerializeObject(ex), StaticVariables.FlgLogger);

                ListAutoSuggest = new C3MethodResponse<List<AutoSuggestResponse>>()
                {
                    code = (int)HttpStatusCode.InternalServerError,
                    message = "Ocurri� un error en la b�squeda por palabra.",
                    more_info = JsonConvert.SerializeObject(ex)
                };

                response.StatusCode = (int)HttpStatusCode.InternalServerError;
                response.IsBase64Encoded = false;
                response.Body = JsonConvert.SerializeObject(ListAutoSuggest);
            }

            loggerLambda.ToWriterLoggs("-----------------------------------ENDLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
            return response;

        }
    }
}
