﻿using System;
using System.Collections.Generic;
using System.Text;
using ECOM.AWS.CatalogoExtendido.Services;
using Newtonsoft.Json;
using System.Dynamic;

namespace ECOM.AWS.CatalogoExtendido.Business
{
    public class BizCancelAE
    {
        public static object SetCancelAE(object info)
        {
            try
            {
                var response = WSCancelAE.SetCancelAE(info);

                return response;
            }
            catch (Exception ex)
            {
                dynamic error = new ExpandoObject();

                error.Code = 636873087798863492;
                error.Message = "Ocurrio un error al ejecutar la cancelación AE";
                error.Result = null;

                return JsonConvert.SerializeObject(error);
            }
        }
    }
}
