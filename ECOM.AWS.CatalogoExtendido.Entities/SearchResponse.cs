﻿using System;
using System.Collections.Generic;
using System.Text;


namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class AutoSuggestResponse
    {
        public long productId { get; set; }        
        public string productName { get; set; }
        public int inventory { get; set; }
        public string imageUrl { get; set; }
    }
    public class ProductSearchItems
    {
        public string ItemId { get; set; }
        public List<ImagesInfo> Images { get; set; }
        public List<CommertialOfferInfo> Sellers { get; set; }
    }
    public class PagedSearchResponse
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public List<string> AbonoPrecios { get; set; }
        public List<string> Modelo { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }        
        public decimal ListPrice { get; set; }        
        public decimal PriceWithoutDiscount { get; set; }        
        public int AvailableQuantity { get; set; }
        public string ImageUrl { get; set; }
        public ProductCreditInfo CreditInfo { get; set; }
    }
}
