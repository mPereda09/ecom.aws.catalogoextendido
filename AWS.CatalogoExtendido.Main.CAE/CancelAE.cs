using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Dynamic;
using Newtonsoft.Json;

using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;

using ECOM.AWS.CatalogoExtendido.Business;
using AWS.CatalogoExtendido.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AWS.CatalogoExtendido.Main.CAE
{
    public class CancelAE
    {

        public APIGatewayProxyResponse FuncSetCancelAECompHan(APIGatewayProxyRequest request, ILambdaContext context)
        {
            object response = null;
            try
            {
                Dictionary<string, string> stageVariables = request.StageVariables != null ? new Dictionary<string, string>(request.StageVariables, StringComparer.OrdinalIgnoreCase) : null;

                StaticVariables.SetStageVariables(stageVariables);

                response = BizCancelAE.SetCancelAE(request.Body);
            }
            catch (Exception ex)
            {
                dynamic error = new ExpandoObject();

                error.Code = 636873089482203286;
                error.Message = "Ocurrio un error al ejecutar la cancelación AE.";
                error.Result = null;

                response = JsonConvert.SerializeObject(error);
            }
            return new APIGatewayProxyResponse()
            {
                Body = response.ToString(),
                IsBase64Encoded = false,
                StatusCode = (int)HttpStatusCode.OK,
            };
        }
    }
}
