﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.CatalogoExtendido.Entities.Interfaces
{
    public interface IC3MethodResponse
    {
        int code { get; set; }
        string message { get; set; }
        string more_info { get; set; }
    }
}
