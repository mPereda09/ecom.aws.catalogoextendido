﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class VtexApiResponse
    {
        public string Content { get; set; }
        public int HttpStatusCode { get; set; }
    }
}
