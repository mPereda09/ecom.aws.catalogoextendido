using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using ECOM.AWS.CatalogoExtendido.Business;
using ECOM.AWS.CatalogoExtendido.Entities;
using Newtonsoft.Json;
using System.Net;
using AWS.CatalogoExtendido.Core;


// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AWS.CatalogoExtendido.Main.SkuDetail
{
    public class FunctionSkuDetail
    {
        public APIGatewayProxyResponse FunctionSkuDetailHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            C3MethodResponse<ProductDetail> prodResponse = null;
            APIGatewayProxyResponse response = new APIGatewayProxyResponse();
            var loggerLambda = new LoggerLambda();
            try
            {
                Dictionary<string, string> queryParams = request.PathParameters != null ? new Dictionary<string, string>(request.PathParameters, StringComparer.OrdinalIgnoreCase) : null;
                Dictionary<string, string> stageVariables = request.StageVariables != null ? new Dictionary<string, string>(request.StageVariables, StringComparer.OrdinalIgnoreCase) : null;
                StaticVariables.SetStageVariables(stageVariables);

                loggerLambda.ToWriterLoggs("-----------------------------------STARTLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                loggerLambda.ToWriterLoggs("REQUEST || " + "INFO ||" + JsonConvert.SerializeObject(request.Body), StaticVariables.FlgLogger);
                BizProductDetail bizCtx = new BizProductDetail();
                prodResponse = bizCtx.GetProductInfo(queryParams);

                response.Body = JsonConvert.SerializeObject(prodResponse);
                response.IsBase64Encoded = false;
                response.StatusCode = (int)HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                loggerLambda.ToWriterLoggs("-----------------------------------EXCEPTIONLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                loggerLambda.ToWriterLoggs("ERROR-EXCEPTION || " + "INFO ||" + JsonConvert.SerializeObject(ex), StaticVariables.FlgLogger);

                prodResponse = new C3MethodResponse<ProductDetail>()
                {
                    code = (int)HttpStatusCode.InternalServerError,
                    message = "Ocurri� un error en la b�squeda del detalle de producto.",
                    more_info = null
                };

                response.Body = JsonConvert.SerializeObject(prodResponse);
                response.IsBase64Encoded = false;
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            loggerLambda.ToWriterLoggs("-----------------------------------ENDLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
            return response;
        }
    }
}
