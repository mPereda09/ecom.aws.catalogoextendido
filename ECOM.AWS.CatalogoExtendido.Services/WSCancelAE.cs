﻿using System;
using System.Collections.Generic;
using System.Text;
using AWS.CatalogoExtendido.Core;
using RestSharp;

namespace ECOM.AWS.CatalogoExtendido.Services
{
    public class WSCancelAE
    {
        public static string SetCancelAE(object info)
        {
            if (string.IsNullOrWhiteSpace(StaticVariables.Variables["UrlCancelAE"])) throw new Exception("No se obtuvo url para cancelación AE.");
            if (string.IsNullOrWhiteSpace(StaticVariables.Variables["CancelAppKey"]) || string.IsNullOrWhiteSpace(StaticVariables.Variables["CancelAppToken"])) throw new Exception("No se obtuvieron los accesos al servicio de cancelación AE");
            

            string urlCancelAE = StaticVariables.Variables["UrlCancelAE"];
            string cancelAppKey = StaticVariables.Variables["CancelAppKey"];
            string cancelAppToken = StaticVariables.Variables["CancelAppToken"];

            var client = new RestClient(urlCancelAE);

            var request = new RestRequest(Method.POST);

            request.AddHeader("EcomAppKey", cancelAppKey);
            request.AddHeader("EcomAppToken", cancelAppToken);

            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(info);

            var response = client.Execute(request);

            if (!response.IsSuccessful)
            {
                throw new Exception(response.StatusDescription);
            }

            return response.Content;
        }
    }
}
