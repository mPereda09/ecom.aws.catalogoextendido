﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using AWS.CatalogoExtendido.Core;
using ECOM.AWS.CatalogoExtendido.Entities;
using ECOM.AWS.CatalogoExtendido.Logger;
using MethodResponse;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using MethodResponse;
using System.Net;

namespace ECOM.AWS.CatalogoExtendido.Services
{
    public class WSPaymentComplement
    {
        public static string SetPaymentComplement(object info)
        {
            if (string.IsNullOrWhiteSpace(StaticVariables.Variables["UrlPaymentComplement"])) throw new Exception("No se obtuvo url para complemento de pago.");
            if (string.IsNullOrWhiteSpace(StaticVariables.Variables["PayCompUser"]) || string.IsNullOrWhiteSpace(StaticVariables.Variables["PayCompPwd"])) throw new Exception("No se obtuvieron los accesos al servicio de complemento de pago");

            string urlPayment = StaticVariables.Variables["UrlPaymentComplement"];
            string usrPayment = StaticVariables.Variables["PayCompUser"];
            string pwdPayment = StaticVariables.Variables["PayCompPwd"];

            var client = new RestClient(urlPayment);

            var request = new RestRequest(Method.POST);

            var paymentAuthBytes = Encoding.UTF8.GetBytes(string.Format("{0}:{1}", usrPayment, pwdPayment));
            var wsPaymentAuth = Convert.ToBase64String(paymentAuthBytes);
            request.AddHeader("Authorization", wsPaymentAuth);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(info);

            var response = client.Execute(request);

            if (!response.IsSuccessful)
            {
                throw new Exception(response.StatusDescription);
            }

            return response.Content;
        }

        public static MethodResponse<List<ResponsePayment>> SetPaymentComplementCE(object infoPaymentComplement)
        {
            MethodResponse<List<ResponsePayment>> response = new MethodResponse<List<ResponsePayment>>();

            var loggerLambda = new LoggerLambda();
            string payRequest = string.Empty;
            string infoRequest = string.Empty;

            string stringBody = infoPaymentComplement.ToString();
            if (string.IsNullOrWhiteSpace(StaticVariables.Variables["PayCompUser"]) || string.IsNullOrWhiteSpace(StaticVariables.Variables["PayCompPwd"])) throw new Exception("No se obtuvieron los accesos al servicio de complemento de pago");

            string urlPayment = StaticVariables.Variables["UrlPaymentComplementCE"];//UrlPaymentComplementCE
            string usrPayment = StaticVariables.Variables["PayCompUser"];
            string pwdPayment = StaticVariables.Variables["PayCompPwd"];

            try
            {
                if (!string.IsNullOrEmpty(stringBody))
                {
                    var jsonSerialice = JsonConvert.DeserializeObject<ComplementPaymentCe>(stringBody);
                    var parseJson = new ComplementPaymentCeObject();
                    parseJson.presupuesto = new List<Info>();
                    parseJson.Paymentcomplement = jsonSerialice.Paymentcomplement;
                    parseJson.presupuesto.Add(jsonSerialice.Info);

                    var jsoString = JsonConvert.SerializeObject(parseJson, Formatting.Indented);

                    var client = new RestClient(urlPayment);
                    var request = new RestRequest(Method.POST);

                    var paymentAuthBytes = Encoding.UTF8.GetBytes(string.Format("{0}:{1}", usrPayment, pwdPayment));
                    var wsPaymentAuth = Convert.ToBase64String(paymentAuthBytes);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Authorization", wsPaymentAuth);
                    request.RequestFormat = DataFormat.Json;
                    request.AddJsonBody(parseJson);

                    var responseClient = client.Execute(request);
                    if (!responseClient.IsSuccessful)
                    {
                        throw new Exception(responseClient.StatusDescription);
                    }
                    var jsonSerialResult = JsonConvert.DeserializeObject<MethodResponse<List<ResponsePayment>>>(responseClient.Content);

                    if (jsonSerialResult.Code != 0)
                    {
                        loggerLambda.ToWriterLoggs("-----------------------------------ERRORSERVICE-PAYMENT-----------------------------------------------------------", StaticVariables.FlgLogger);
                        loggerLambda.ToWriterLoggs("ERRORSERVICE-PAYMENT || " + "INFO ||" + "Ocucrrio un error al intentarse conectar al servicio de paymentComplement || " + JsonConvert.SerializeObject(jsonSerialResult), StaticVariables.FlgLogger);
                        jsonSerialResult.Message = "Ocurrio un Error al intentar Realizar el Pago";
                    }

                    response = jsonSerialResult;
                }
            }
            catch (WebException wex) {
                loggerLambda.ToWriterLoggs("-----------------------------------EXCEPTIONLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                loggerLambda.ToWriterLoggs("WebException || " + "INFO ||" + JsonConvert.SerializeObject(wex), StaticVariables.FlgLogger);

                response.Code = -2;
                response.Message = JsonConvert.SerializeObject(wex);//"Ocurrio un Error al intentar Realizar el Pago";
                response.Result = null;
            }
            catch (Exception ex)
            {
                loggerLambda.ToWriterLoggs("-----------------------------------EXCEPTIONLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                loggerLambda.ToWriterLoggs("ERROR-EXCEPTION || " + "INFO ||" + JsonConvert.SerializeObject(ex), StaticVariables.FlgLogger);

                response.Code = -1;
                response.Message = "Ocurrio un Error al intentar Realizar el Pago";
                response.Result = null;
            }

            return response;

        }

    }
}
