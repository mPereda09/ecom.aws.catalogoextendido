﻿using Amazon.Lambda.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.CatalogoExtendido.Business
{
    public class LoggerLambda
    {
        public void ToWriterLoggs(object log, string flag)
        {
            var parseFlag = Convert.ToBoolean(flag);

            if (parseFlag)
            {
                LambdaLogger.Log("-----------------------------------" + log + "-----------------------------------------------------------");
            }
        }
    }
}
