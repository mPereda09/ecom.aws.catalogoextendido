﻿using ECOM.AWS.CatalogoExtendido.Entities.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class ProductDetailResponse
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string InfoError { get; set; }
        public List<C3MethodResponse<ProductDetail>> Products { get; set; }

    }

}
