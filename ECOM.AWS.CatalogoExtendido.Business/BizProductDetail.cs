﻿using System;
using System.Collections.Generic;
using System.Text;
using ECOM.AWS.CatalogoExtendido.Entities;
using ECOM.AWS.CatalogoExtendido.Services;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using AWS.CatalogoExtendido.Core;
using Newtonsoft.Json.Linq;
using Amazon.Lambda.Core;

namespace ECOM.AWS.CatalogoExtendido.Business
{
    public class BizProductDetail
    {
        const string kElektraSellerId = "1";
        const string kAvoidCatKey = "avoidcategories";
        const string kKeyWord = "sku";
        const int kAbonoPreciosId = 994;
        const int kForzarPrecioId = 983;


        #region Public methods
        public C3MethodResponse<ProductDetail> GetProductInfo(Dictionary<string, string> pathParameters)
        {
            var responseC3 = new C3MethodResponse<ProductDetail>();

            if (pathParameters == null || (pathParameters != null && !pathParameters.ContainsKey(kKeyWord)))
            {
                throw new ArgumentException("No se específico un sku para la búsqueda.");
            }
            string keyword = pathParameters[kKeyWord].Trim();

            try
            {
                var wsProd = new WSProduct();
                var searchByResponse = wsProd.ProductDetail(keyword);
                var jsonCount = JsonConvert.DeserializeObject<VtexApiResponse>(searchByResponse.Content);
                searchByResponse.Content = searchByResponse.Content.Replace("Customer Bennefits", "Beneficios del cliente").Replace("Customer Benefits", "Beneficios del cliente");
                var prodDetail = JsonConvert.DeserializeObject<ProductDetail>(searchByResponse.Content);
                List<string> invalidCategories = new List<string>();

                foreach (var kvp in StaticVariables.Variables)
                {
                    if (kvp.Key.ToLower().Contains(kAvoidCatKey))
                    {
                        byte[] dataCat = System.Convert.FromBase64String(kvp.Value);
                        var cat = System.Text.Encoding.UTF8.GetString(dataCat);
                        invalidCategories.AddRange(cat.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList<string>());
                    }
                }

                var listProd = prodDetail.ProductCategories.Where(pc => invalidCategories.Contains(pc.Key)).ToList();

                if (listProd.Count == 0)
                {
                    if (prodDetail != null)
                    {
                        int sku = Convert.ToInt32(prodDetail.ProductId);
                        //var prodCommertialOfferInfo = GetProductCommertialOfferInfo(sku);
                        var getListPrice = wsProd.DetailPricebySku(prodDetail.ProductId);

                        prodDetail.AvailableQuantity = getListPrice.Count == 0 ? 0 : wsProd.ProductInventory(Convert.ToString(prodDetail.ProductId));
                        prodDetail.ListPrice = getListPrice.Count == 0 ? 0 : Convert.ToDecimal(getListPrice[0].Items[0].Sellers[0].CommertialOffer.ListPrice);
                        prodDetail.Price = getListPrice.Count == 0 ? 0 : Convert.ToDecimal(getListPrice[0].Items[0].Sellers[0].CommertialOffer.Price); ;

                        var specAbonoPrecios = prodDetail.ProductSpecifications.Where(ps => ps.FieldId == kAbonoPreciosId).FirstOrDefault();
                        string abonoPrecios = null;

                        if (specAbonoPrecios != null && specAbonoPrecios.FieldValues.Count > 0)
                        {
                            abonoPrecios = specAbonoPrecios.FieldValues[0];
                            prodDetail.ProductSpecifications.Remove(specAbonoPrecios);
                        }

                        var specForzarPrecio = prodDetail.ProductSpecifications.Where(ps => ps.FieldId == kForzarPrecioId).FirstOrDefault();

                        if (specForzarPrecio != null)
                        {
                            prodDetail.ProductSpecifications.Remove(specForzarPrecio);
                        }

                        prodDetail.CreditInfo = CalculateCreditInfo(getListPrice.Count == 0 ? 0 : getListPrice[0].Items[0].Sellers[0].CommertialOffer.Price, abonoPrecios);

                        responseC3.code = (int)HttpStatusCode.OK;
                        responseC3.message = null;
                        responseC3.more_info = null;
                        responseC3.result = prodDetail;
                    }
                    else
                    {
                        throw new Exception("Ocurrió un error en la búsqueda del detalle de producto.");
                    }
                }
            }

            catch (Exception ex)
            {
                responseC3.code = (int)HttpStatusCode.InternalServerError;
                responseC3.message = "Ocurrió un error en la búsqueda del detalle de producto.";
                responseC3.more_info = null;
            }

            return responseC3;
        }

        public static ProductCreditInfo CalculateCreditInfo(decimal bestPrice, string abonoPrecios)
        {
            int weeks = 0;
            decimal interest = 0;
            decimal totalWithInterest = 0;
            int timelyPayment = 0;
            string infoAbonoPrecios = string.Empty;

            if (!string.IsNullOrWhiteSpace(abonoPrecios))
            {
                if (abonoPrecios.Contains("{"))
                {
                    var parseJson = abonoPrecios != null ? JObject.Parse(abonoPrecios) : null;// {}   {"weeks":128,"weekly":331}

                    if (parseJson != null)
                    {
                        var week = parseJson.GetValue("weeks");
                        if (week != null)
                        {
                            abonoPrecios = "0.5340|17.90|" + week;
                        }
                        else
                        {
                            abonoPrecios = null;
                        }
                    }
                }

            }

            if (!string.IsNullOrWhiteSpace(abonoPrecios) && Convert.ToDecimal(abonoPrecios.Split("|")[0]) > 0)
            {
                infoAbonoPrecios = abonoPrecios;
            }
            else
            {
                if (bestPrice > 280)
                {
                    if (bestPrice < 1500)
                    {
                        infoAbonoPrecios = StaticVariables.CalculoSinAbonoMenor1500;
                    }
                    else
                    {
                        infoAbonoPrecios = StaticVariables.CalculoSinAbonoMayor1500;
                    }
                    infoAbonoPrecios = infoAbonoPrecios.Replace("_", "|");
                }
            }

            var abonosInfo = infoAbonoPrecios.Split("|");

            if (abonosInfo != null && abonosInfo.Length > 2)
            {
                weeks = Convert.ToInt32(abonosInfo[2]);
                interest = bestPrice * Convert.ToDecimal(abonosInfo[0]);
                totalWithInterest = bestPrice + interest;
                timelyPayment = Convert.ToInt32(Math.Ceiling(Math.Round(totalWithInterest / weeks, MidpointRounding.AwayFromZero)));
            }

            return new ProductCreditInfo()
            {
                PagoPuntual = timelyPayment,
                PlazoMaximo = weeks
            };
        }
        #endregion

        #region Private methods
        private CommertialOfferInfo GetProductCommertialOfferInfo(int sku)
        {
            if (sku <= 0) throw new ArgumentException("Se debe especificar un sku válido.");

            WSProduct wsProd = new WSProduct();
            var prodVar = wsProd.SearchBy(sku.ToString(), null);

            var result = JsonConvert.DeserializeObject<List<VtexProductsSearchResponse>>(prodVar.Content);

            var commertialOffer = (from ps in result
                                   where ps.ProductId == sku
                                   from itm in ps.Items
                                   where itm.ItemId == sku.ToString()
                                   from sll in itm.Sellers
                                   where sll.SellerId == kElektraSellerId
                                   select sll.CommertialOffer).First();

            return commertialOffer;
        }
        #endregion
    }
}
