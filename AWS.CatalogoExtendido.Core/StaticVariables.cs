﻿using System;
using System.Collections.Generic;

namespace AWS.CatalogoExtendido.Core
{
    public static class StaticVariables
    {
        public static string VtexApiKey { get; set; }
        public static string VtexApiToken { get; set; }
        public static string VtexApiDomain { get; set; }
        public static string CalculoSinAbonoMayor1500 { get; set; }
        public static string CalculoSinAbonoMenor1500 { get; set; }
        public static Dictionary<string, string> Variables { get; set; }
        public static string Vtexcommercestable { get; set; }
        public static string UrlPaymentComplement { get; set; }
        public static string PayCompPwd { get; private set; }
        public static string PayCompUser { get; private set; }
        public static string An { get; set; }
        public static string FlgLogger { get; set; }
        public static string Nproducts { get; set; }
        public static string UrlPaymentComplementCE { get; set; }
        public static void SetStageVariables(Dictionary<string, string> variables)
        {
            if (variables != null)
            {
                Variables = variables;
                VtexApiKey = variables["VtexApiAppKey"];
                VtexApiToken = variables["VtexApiAppToken"];
                VtexApiDomain = variables["VtexApiDomain"];
                CalculoSinAbonoMayor1500 = variables["CredCalcSinAbonoMy1500"];
                CalculoSinAbonoMenor1500 = variables["CredCalcSinAbonoMn1500"];
                CalculoSinAbonoMenor1500 = variables["CredCalcSinAbonoMn1500"];
                UrlPaymentComplement = variables["UrlPaymentComplement"];
                PayCompUser = variables["PayCompUser"];
                PayCompPwd = variables["PayCompPwd"];
                Vtexcommercestable = variables["Vtexcommercestable"];
                An = variables["An"];
                FlgLogger = variables["FlgLogger"];
                Nproducts = variables["Nproducts"];
                UrlPaymentComplementCE = variables["UrlPaymentComplementCE"];
            }
            else
            {
                throw new Exception("No se encontrarón variables de etapa configuradas.");
            }
        }
    }
}
