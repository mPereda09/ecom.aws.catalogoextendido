using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using AWS.CatalogoExtendido.Core;
using ECOM.AWS.CatalogoExtendido.Business;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace ECOM.AWS.CatalogoExtendido.Main.PaymentComplementCE
{
    public class function
    {

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public APIGatewayProxyResponse SetPaymentCE(APIGatewayProxyRequest request, ILambdaContext context)
        {
            APIGatewayProxyResponse response = new APIGatewayProxyResponse();
            BzPayComplementCE payComBz = new BzPayComplementCE();
            var loggerLambda = new LoggerLambda();
            try
            {
                Dictionary<string, string> stageVariables = request.StageVariables != null ? new Dictionary<string, string>(request.StageVariables, StringComparer.OrdinalIgnoreCase) : null;
                StaticVariables.SetStageVariables(stageVariables);
                loggerLambda.ToWriterLoggs("-----------------------------------STARTLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                loggerLambda.ToWriterLoggs("REQUEST || " + "INFO ||" + JsonConvert.SerializeObject(request.Body), StaticVariables.FlgLogger);
                var bodyResponse = payComBz.PaymentComplement(request.Body);

                loggerLambda.ToWriterLoggs("RESPONSE || " + "INFO ||" + JsonConvert.SerializeObject(bodyResponse), StaticVariables.FlgLogger);
                response.IsBase64Encoded = true;
                response.StatusCode = (int)HttpStatusCode.OK;
                response.Body = bodyResponse;
            }
            catch (Exception ex)
            {
                loggerLambda.ToWriterLoggs("-----------------------------------EXCEPTIONLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                loggerLambda.ToWriterLoggs("ERROR-EXCEPTION || " + "INFO ||" + JsonConvert.SerializeObject(ex), StaticVariables.FlgLogger);

                response = new APIGatewayProxyResponse()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsBase64Encoded = false,
                    Body = JsonConvert.SerializeObject(ex.Message)  
                };

            }

            loggerLambda.ToWriterLoggs("-----------------------------------ENDLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
            return response;
        }
    }
}
