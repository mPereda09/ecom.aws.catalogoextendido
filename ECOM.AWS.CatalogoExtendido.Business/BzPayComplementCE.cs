﻿using ECOM.AWS.CatalogoExtendido.Services;
using Newtonsoft.Json;
using System;
using System.Dynamic;
using MethodResponse;
using ECOM.AWS.CatalogoExtendido.Entities;
using System.Collections.Generic;

namespace ECOM.AWS.CatalogoExtendido.Business
{
    public class BzPayComplementCE
    {
        public string PaymentComplement(object stringRequest)
        {
            try
            {
                MethodResponse<List<ResponsePayment>> response = new MethodResponse<List<ResponsePayment>>();
                response = WSPaymentComplement.SetPaymentComplementCE(stringRequest);
                var returnJson = JsonConvert.SerializeObject(response);
                return returnJson;
            }
            catch (Exception ex)
            {
                dynamic error = new ExpandoObject();

                error.Code = 636873088449233257;
                error.Message = "Ocurrio un error al ejecutar el complemento de pago";
                error.Result = null;

                return JsonConvert.SerializeObject(error);
            }
        }
    }
}