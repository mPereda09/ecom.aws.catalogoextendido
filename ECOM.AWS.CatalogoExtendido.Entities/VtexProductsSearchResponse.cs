﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class VtexProductsSearchResponse
    {
        [JsonProperty(PropertyName = "productId")]
        public long ProductId { get; set; }

        [JsonProperty(PropertyName = "productName")]
        public string ProductName { get; set; }

        [JsonProperty(PropertyName = "brand")]
        public string Brand { get; set; }

        [JsonProperty(PropertyName = "Abonoprecios")]
        public List<string> AbonoPrecios { get; set; }

        [JsonProperty(PropertyName = "Modelo")]
        public List<string> Modelo { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "items")]
        public List<ItemsInfo> Items { get; set; }

        [JsonProperty(PropertyName = "categoryId")]
        public string CategoryId { get; set; }
    }
}
