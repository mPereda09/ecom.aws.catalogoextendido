﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class SkuRequest
    {
        [JsonProperty("Skus")]
        public List<long> Skus { get; set; }
    }
}
