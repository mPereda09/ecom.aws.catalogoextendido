﻿using ECOM.AWS.CatalogoExtendido.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using ECOM.AWS.CatalogoExtendido.Entities.Interfaces;

namespace ECOM.AWS.CatalogoExtendido.Business
{
    public class BzProductsDetailAll
    {
        public ProductDetailResponse GetProductsDetail(string BodyRequest)
        {
            BizProductDetail BzSkuDetail = new BizProductDetail();
            C3MethodResponse<ProductDetail> ProductType = new C3MethodResponse<ProductDetail>();
            C3MethodResponse<ProductDetail> ProductTypeNull = new C3MethodResponse<ProductDetail>();
            ProductDetailResponse ProductResponse = new ProductDetailResponse();
            C3MethodResponse<ProductDetail> xvalue = new C3MethodResponse<ProductDetail>();
            ProductResponse.Products = new List<C3MethodResponse<ProductDetail>>();
            try
            {
                SkuRequest skus = JsonConvert.DeserializeObject<SkuRequest>(BodyRequest);
                var UniqueSkus = (from a in skus.Skus select a).Distinct();

                foreach (var sku in UniqueSkus)
                {
                    var DictionarySku = new Dictionary<string, string>();
                    DictionarySku.Add("sku", sku.ToString());
                    ProductType = BzSkuDetail.GetProductInfo(DictionarySku);
                    //xvalue = BzSkuDetail.GetProductInfo(DictionarySku);
                    if (ProductType.result != null)
                    {
                        ProductResponse.Products.Add(ProductType);
                    }
                }

                ProductResponse.Code = (int)HttpStatusCode.OK;

                if (ProductResponse.Products.Count == 0)
                {
                    ProductResponse.Message = "No se encontraron Productos"/*+JsonConvert.SerializeObject(xvalue)*/;
                    ProductResponse.InfoError = null;
                    ProductResponse.Products = null;
                }

            }
            catch (Exception ex)
            {
                ProductResponse.Code = (int)HttpStatusCode.InternalServerError;
                ProductResponse.Message = ex.Message;
                ProductResponse.InfoError = ex.Data.ToString();
                ProductResponse.Products = null;
            }
            var xjson = JsonConvert.SerializeObject(ProductResponse);
            return ProductResponse;
        }
    }
}