﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class SkuVariations
    {
        [JsonProperty(PropertyName = "sku")]
        public int Sku { get; set; }

        [JsonProperty(PropertyName = "availablequantity")]
        public int Availablequantity { get; set; }

        [JsonProperty(PropertyName = "listPrice")]
        public decimal ListPrice { get; set; }

        [JsonProperty(PropertyName = "bestPrice")]
        public decimal BestPrice { get; set; }

        [JsonProperty(PropertyName = "sellerId")]
        public string SellerId { get; set; }
    }

    public class VtexProductVariations
    {
        [JsonProperty(PropertyName = "productId")]
        public int ProductId { get; set; }

        [JsonProperty(PropertyName = "skus")]
        public List<SkuVariations> Skus { get; set; }
    }
}
