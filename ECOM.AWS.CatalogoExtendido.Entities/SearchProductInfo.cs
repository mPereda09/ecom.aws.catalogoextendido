﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class ImagesInfo
    {
        [JsonProperty(PropertyName = "imageUrl")]
        public string imageUrl { get; set; }

        [JsonProperty(PropertyName = "imageText")]
        public string imageText { get; set; }
    }

    public class CommertialOfferInfo
    {
        [JsonProperty(PropertyName = "Price")]
        public decimal Price { get; set; }

        [JsonProperty(PropertyName = "ListPrice")]
        public decimal ListPrice { get; set; }

        [JsonProperty(PropertyName = "PriceWithoutDiscount")]
        public decimal PriceWithoutDiscount { get; set; }

        [JsonProperty(PropertyName = "AvailableQuantity")]
        public int AvailableQuantity { get; set; }
    }

    public class SellersInfo
    {
        [JsonProperty(PropertyName = "sellerId")]
        public string SellerId { get; set; }

        [JsonProperty(PropertyName = "commertialOffer")]
        public CommertialOfferInfo CommertialOffer { get; set; }
    }

    public class ItemsInfo
    {
        [JsonProperty(PropertyName = "itemId")]
        public string ItemId { get; set; }

        [JsonProperty(PropertyName = "images")]
        public List<ImagesInfo> Images { get; set; }

        [JsonProperty(PropertyName = "sellers")]
        public List<SellersInfo> Sellers { get; set; }
    }
}
