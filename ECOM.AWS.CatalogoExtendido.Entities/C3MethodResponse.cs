﻿using System;
using ECOM.AWS.CatalogoExtendido.Entities.Interfaces;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class C3MethodResponse<T>: IC3MethodResponse
    {
        public int code { get; set; }
        public string message { get; set; }
        public string more_info { get; set; }
        public T result { get; set; }
    }
}
