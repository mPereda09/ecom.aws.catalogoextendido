using System;
using System.Collections.Generic;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using ECOM.AWS.CatalogoExtendido.Business;
using ECOM.AWS.CatalogoExtendido.Entities;
using Newtonsoft.Json;
using System.Net;
using AWS.CatalogoExtendido.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AWS.CatalogoExtendido.Main.PagedSearch
{
    public class FunctionPagedSearch
    {
        public APIGatewayProxyResponse FunctionPagedSearchHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            C3MethodResponse<List<PagedSearchResponse>> searchResponse = null;
            APIGatewayProxyResponse response = new APIGatewayProxyResponse();
            var loggerLambda = new LoggerLambda();
            string resources = string.Empty;
            try
            {
                Dictionary<string, string> queryParams = request.QueryStringParameters != null ? new Dictionary<string, string>(request.QueryStringParameters, StringComparer.OrdinalIgnoreCase) : null;
                Dictionary<string, string> stageVariables = request.StageVariables != null ? new Dictionary<string, string>(request.StageVariables, StringComparer.OrdinalIgnoreCase) : null;
                StaticVariables.SetStageVariables(stageVariables);

                loggerLambda.ToWriterLoggs("-----------------------------------STARTLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                BizProductSearch pageSearchbz = new BizProductSearch();
                searchResponse = pageSearchbz.PagedSearch(queryParams, out resources);

                response.Body = JsonConvert.SerializeObject(searchResponse);
                response.IsBase64Encoded = true;
                response.StatusCode = (int)HttpStatusCode.PartialContent;

            }
            catch (Exception ex)
            {
                loggerLambda.ToWriterLoggs("-----------------------------------EXCEPTIONLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
                loggerLambda.ToWriterLoggs("ERROR-EXCEPTION || " + "INFO ||" + JsonConvert.SerializeObject(ex), StaticVariables.FlgLogger);

                searchResponse = new C3MethodResponse<List<PagedSearchResponse>>()
                {
                    code = (int)HttpStatusCode.InternalServerError,
                    message = "Ocurri� un error en la b�squeda de producto con paginaci�n.",
                    more_info = null
                };

                response.Body = JsonConvert.SerializeObject(searchResponse);
                response.IsBase64Encoded = false;
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

            loggerLambda.ToWriterLoggs("-----------------------------------ENDLOG-----------------------------------------------------------", StaticVariables.FlgLogger);
            return response;
        }
    }
}
