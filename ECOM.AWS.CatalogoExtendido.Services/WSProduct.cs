﻿using System;
using RestSharp;
using System.Collections.Generic;
using System.Text;
using ECOM.AWS.CatalogoExtendido.Entities;
using System.Linq;
using System.Net;
using AWS.CatalogoExtendido.Core;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Amazon.Lambda.Core;
using ECOM.AWS.CatalogoExtendido.Logger;

namespace ECOM.AWS.CatalogoExtendido.Services
{
    public class WSProduct
    {
        #region Public Methods
        public VtexApiResponse SearchBy(string keyword, Dictionary<string, string> queryStringParams)
        {
            if (string.IsNullOrWhiteSpace(StaticVariables.VtexApiDomain)) throw new Exception("No se obtuvo el dominio de api VTEX.");

            string urlVar = StaticVariables.VtexApiDomain + "/api/catalog_system/pub/products/search";

            string urlRequest = urlVar + "?ft=" + keyword;

            if (queryStringParams != null && queryStringParams.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var qParam in queryStringParams)
                {
                    sb.Append(string.Format("&{0}={1}", qParam.Key, qParam.Value));
                }
                urlRequest += sb.ToString();
            }

            var client = new RestClient(urlRequest);

            var request = GetVtexRequest(Method.GET);
            var response = client.Execute(request);

            if (!response.IsSuccessful)
            {
                throw new Exception(response.StatusDescription);
            }

            return new VtexApiResponse()
            {
                Content = response.Content,
                HttpStatusCode = (int)response.StatusCode
            };
        }

        public List<VtexProductsSearchResponse> AutoSuggestSearchBy(string keyword, Dictionary<string, string> queryStringParams, string kKeyInventory)
        {
            var loggerLambda = new LoggerLambda();
            loggerLambda.ToWriterLoggs("START-SearchProduct", StaticVariables.FlgLogger);

            var valueDictionary = new Dictionary<string, string>();
            int nIndex = 0;
            int nPag = 49;
            string searchResources = string.Empty;
            valueDictionary.Add("ft", keyword);
            valueDictionary.Add("_from", Convert.ToString(nIndex));
            valueDictionary.Add("_to", Convert.ToString(nPag));
            var listResponse = new List<VtexProductsSearchResponse>();


            if (string.IsNullOrWhiteSpace(StaticVariables.VtexApiDomain)) throw new Exception("No se obtuvo el dominio de api VTEX.");
            var productResponse = SearchWithPaging(valueDictionary, out searchResources);
            var results = JsonConvert.DeserializeObject<List<VtexProductsSearchResponse>>(productResponse.Content);

            try
            {
                if (results.Count > 0)
                {
                    int indexList = 1;
                    foreach (var invProduct in results)
                    {
                        var inv = invProduct.Items[0].Sellers[0].CommertialOffer.AvailableQuantity = ProductInventory(Convert.ToString(invProduct.ProductId));

                        if (indexList <= Convert.ToInt32(StaticVariables.Nproducts))
                        {
                            if (Convert.ToBoolean(kKeyInventory))
                            {
                                if (inv > 0)
                                {
                                    listResponse.Add(invProduct);
                                    indexList += 1;
                                }
                            }
                            else
                            {
                                listResponse.Add(invProduct);
                                indexList += 1;

                            }

                        }
                        else
                        {
                            break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                loggerLambda.ToWriterLoggs("ERROR-EXCEPTION :" + JsonConvert.SerializeObject(ex), StaticVariables.FlgLogger);
            }

            loggerLambda.ToWriterLoggs("End-SearchProduct", StaticVariables.FlgLogger);
            return listResponse;
        }

        public VtexApiResponse SearchWithPaging(Dictionary<string, string> queryStringParams, out string resources)
        {
            resources = null;
            VtexApiResponse resultClient = new VtexApiResponse();
            string urlVar = string.Empty;
            string urlconcat = string.Empty;
            var loggerLambda = new LoggerLambda();

            loggerLambda.ToWriterLoggs("-----------------------------------START-SearchWithPaging------------------------------------------------------------", StaticVariables.FlgLogger);
            if (string.IsNullOrWhiteSpace(StaticVariables.VtexApiDomain)) throw new Exception("No se obtuvo el dominio de api VTEX.");
            queryStringParams.Add("fq", "sellerId:1");//Solo Productos Elektra
            urlVar = StaticVariables.VtexApiDomain + "/api/catalog_system/pub/products/search";
            urlconcat = urlVar + "?";
            loggerLambda.ToWriterLoggs("URL Search|| " + urlconcat, StaticVariables.FlgLogger);  //url Logger

            try
            {

                if (queryStringParams != null && queryStringParams.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var qParam in queryStringParams)
                    {
                        sb.Append(string.Format("{0}={1}&", qParam.Key, qParam.Value));
                    }
                    urlconcat += sb.ToString().Remove(sb.ToString().Length - 1, 1);
                }

                var client = new RestClient(urlconcat);
                var request = GetVtexRequest(Method.GET);
                var response = client.Execute(request);

                loggerLambda.ToWriterLoggs("PetitionClientToVtex |" + "STATUS CODE : " + response.StatusCode, StaticVariables.FlgLogger);  //url Logger

                if (!response.IsSuccessful)
                {
                    throw new Exception(response.StatusDescription);
                }

                resources = response.Headers.Where(hd => string.Equals(hd.Name, "resources")).First().Value.ToString();
                resultClient.Content = response.Content;
                resultClient.HttpStatusCode = (int)response.StatusCode;

            }
            catch (Exception ex)
            {
                loggerLambda.ToWriterLoggs("ERROR-EXCEPTION-SearchWithPaging || " + "INFO ||" + JsonConvert.SerializeObject(ex), StaticVariables.FlgLogger);
            }

            loggerLambda.ToWriterLoggs("END-SearchWithPaging" + urlconcat, StaticVariables.FlgLogger);
            return resultClient;
        }

        public int ProductInventory(string productId)
        {
            int prodInven = 0;
            int warehouseId = 139;

            try
            {
                VtexProductsSearchResponse prodInveResponse = new VtexProductsSearchResponse();

                string urlVar = StaticVariables.Vtexcommercestable + productId + "?an=" + StaticVariables.An;

                var client = new RestClient(urlVar);

                var request = GetVtexRequest(Method.GET);

                var response = client.Execute(request);

                if (!response.IsSuccessful)
                {
                    throw new Exception(response.StatusDescription);
                }

                var productResult = JObject.Parse(response.Content);

                var conver = JsonConvert.SerializeObject(productResult);

                var valueEfc = (from a in productResult.GetValue("balance") where Convert.ToInt32(a["warehouseId"]) == warehouseId select a).FirstOrDefault();

                if (valueEfc != null && !string.IsNullOrWhiteSpace(valueEfc.ToString()))
                {
                    prodInven = Convert.ToInt32(valueEfc["totalQuantity"]) - Convert.ToInt32(valueEfc["reservedQuantity"]);
                }
            }
            catch (Exception ex)
            {
                prodInven = 0;
            }

            return prodInven;
        }

        public VtexApiResponse ProductDetail(string keyword)
        {
            if (string.IsNullOrWhiteSpace(StaticVariables.VtexApiDomain)) throw new Exception("No se obtuvo el dominio de api VTEX.");

            string urlVar = StaticVariables.VtexApiDomain + "/api/catalog_system/pvt/sku/stockkeepingunitbyid/";
            //string urlVar = StaticVariables.VtexApiDomain + "/api/catalog_system/pub/products/search/";

            string urlRequest = urlVar + keyword;
            LambdaLogger.Log("UriSearch |" + urlRequest);

            var client = new RestClient(urlRequest);
            var request = GetVtexRequest(Method.GET);
            var response = client.Execute(request);


            if (!response.IsSuccessful)
            {
                throw new Exception(response.StatusDescription);
            }

            return new VtexApiResponse()
            {
                Content = response.Content,
                HttpStatusCode = (int)response.StatusCode
            };
        }

        public List<DetailProductVtex> DetailPricebySku(string keyword)
        {
            if (string.IsNullOrWhiteSpace(StaticVariables.VtexApiDomain)) throw new Exception("No se obtuvo el dominio de api VTEX.");

            //string urlVar = StaticVariables.VtexApiDomain + "/api/catalog_system/pub/products/search/";
            string urlVar = StaticVariables.VtexApiDomain + "/api/catalog_system/pub/products/search?fq=sellerId:1&ft=";

            string urlRequest = urlVar + keyword;

            var client = new RestClient(urlRequest);

            var request = GetVtexRequest(Method.GET);

            var xson = JsonConvert.SerializeObject(request.Parameters);

            var response = client.Execute(request);


            if (!response.IsSuccessful)
            {
                throw new Exception(response.StatusDescription);
            }

            var results = JsonConvert.DeserializeObject<List<DetailProductVtex>>(response.Content);

            return results;
        }

        public VtexApiResponse ProductVariations(int sku)
        {
            if (string.IsNullOrWhiteSpace(StaticVariables.VtexApiDomain)) throw new Exception("No se obtuvo el dominio de api VTEX.");

            string urlVar = StaticVariables.VtexApiDomain + "/api/catalog_system/pub/products/variations/";

            string urlRequest = urlVar + sku.ToString();

            var client = new RestClient(urlRequest);

            var request = GetVtexRequest(Method.GET);

            var response = client.Execute(request);

            if (!response.IsSuccessful)
            {
                throw new Exception(response.StatusDescription);
            }

            return new VtexApiResponse()
            {
                Content = response.Content,
                HttpStatusCode = (int)response.StatusCode
            };
        }
        #endregion

        #region Private Methods
        private RestRequest GetVtexRequest(Method httpMethod)
        {
            string vtexAppKey = StaticVariables.VtexApiKey;
            string vtexAppToken = StaticVariables.VtexApiToken;

            if (string.IsNullOrWhiteSpace(vtexAppKey) || string.IsNullOrWhiteSpace(vtexAppToken)) throw new Exception("No se obtuvieron los accesos a VTEX.");

            var request = new RestRequest(httpMethod);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");

            request.AddHeader("X-VTEX-API-AppKey", vtexAppKey);
            request.AddHeader("X-VTEX-API-AppToken", vtexAppToken);

            return request;
        }
        #endregion
    }
}
