using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using ECOM.AWS.CatalogoExtendido.Main.Search;
using AWS.CatalogoExtendido.Main.PagedSearch;
using AWS.CatalogoExtendido.Main.SkuDetail;
using AWS.CatalogoExtendido.Main.PaymentComplement;
using AWS.CatalogoExtendido.Main.CAE;
using ECOM.AWS.CatalogoExtendido.Main.PaymentComplementCE;
using AWS.CatalogoExtendido.Main.DetailSkus;

namespace ECOM.AWS.CatalogoExtendido.Tests
{
    public class FunctionTest
    {
        Dictionary<string, string> stageVariables = null;

        public FunctionTest()
        {
            //stageVariables = new Dictionary<string, string>() {
            //    { "CredCalcSinAbonoMn1500", "0.1510_0_13" },
            //    { "CredCalcSinAbonoMy1500", "0.5340_0_52" },
            //    { "VtexApiAppKey", "vtexappkey-elektra-ZGHFGK" },
            //    { "VtexApiAppToken", "XJFFFMYFUHUQVSRDVISCWVCGBTXUWNYMHENPUQNUZZBILXZSGOJMKKDBGYDLPEJHIHIRFBIFVYDXTTUEUEOIDMCXALIVMCEVPUZDPIMEFERMGGHNEZRKPBDWFKQTSMSS" },
            //    { "VtexApiDomain", "https://elektra.vtexcommercestable.com.br" },
            //    { "UrlPaymentComplement", "https://agent.services.elektra.com.mx/Api/ExtendedCatalog/ConfirmPayment" },
            //    { "PayCompUser", "eposTienda" },
            //    { "PayCompPwd", "a9012$QmN41S0S" },
            //    { "UrlCancelAE", "https://agent.services.elektra.com.mx/Service/Api/Post/CatExt/CancelOrder"},
            //    { "CancelAppKey", "AjNpQcFNSxLDwOHERh6iPxyfjaVrRMoViyW2Epg4YIo=" },
            //    { "CancelAppToken", "ncE5fm0MzoMqGqxVC9H5PA==" },
            //    { "AvoidCategories1", "MTM3MTg2OCwxMzcxODY0LDEzNzE4NzAsMTM3MTg2MywxNTg5NDcsMTM3MTg2NywxMzcxODY2LDU1NTMzNSw1NTUzMzYsNTU1MzM5LDU1NTMzOA==" },
            //    { "AvoidCategories2", "MTM3MjExMSwxMjc2NDAsMTI3NjQ1LDI0ODc1MiwxMzcyMDc5" },
            //    { "vtexcommercestable","https://logistics.vtexcommercestable.com.br/api/logistics/pvt/inventory/balance/"},
            //    {"an","elektra" },
            //    {"FlgLogger","false" },
            //    {"Nproducts","5" },
            //    {"UrlPaymentComplementCE","https://agent.services.elektra.com.mx/Api/ExtendedCatalog/ConfirmPaymentMarked" },

            //};


            //    //////QaAllParameters
            stageVariables = new Dictionary<string, string>() {
                            { "CredCalcSinAbonoMn1500", "0.1510_0_13" },
                            { "CredCalcSinAbonoMy1500", "0.5340_0_52" },
                            { "VtexApiAppKey", "vtexappkey-elektraqa-QOUWAD" },
                            { "VtexApiAppToken", "AAEDVKSCLNKGFOJXKYJXIRTOADUALSMMBYDKTLVJEXZHFELEITDFTAHJGQCSPRRSAXEGKZDZCZSLSMIVRQRROHTARUEDIDUMGGGHZFTJPECTIVRTHHAZAZETDDOQUARZ" },
                            { "VtexApiDomain", "https://elektraqa.vtexcommercestable.com.br" },
                            { "UrlPaymentComplement", "https://agent.services.elektra.com.mx/Api/ExtendedCatalog/ConfirmPayment" },
                            { "PayCompUser", "eposTienda" },
                            { "PayCompPwd", "a9012QmN41S0S" },
                            { "UrlCancelAE", "https://agent.services.elektra.com.mx/Service/Api/Post/CatExt/CancelOrder-Prueba"},
                            { "CancelAppKey", "AjNpQcFNSxLDwOHERh6iPxyfjaVrRMoViyW2Epg4YIo=" },
                            { "CancelAppToken", "ncE5fm0MzoMqGqxVC9H5PA==" },
                            { "AvoidCategories1", "MTM3MTg2OCwxMzcxODY0LDEzNzE4NzAsMTM3MTg2MywxNTg5NDcsMTM3MTg2NywxMzcxODY2LDU1NTMzNSw1NTUzMzYsNTU1MzM5LDU1NTMzOA==" },
                            { "AvoidCategories2", "MTM3MjExMSwxMjc2NDAsMTI3NjQ1LDI0ODc1MiwxMzcyMDc5" },
                            { "vtexcommercestable","https://logistics.vtexcommercestable.com.br/api/logistics/pvt/inventory/balance/"},
                            {"an","elektraqa" },
                            {"FlgLogger","false" },
                            {"Nproducts","5" },
                            {"UrlPaymentComplementCE","https://agent.services.elektra.com.mx/Api/ExtendedCatalog/ConfirmPaymentMarked" },

                        };
        }

        [Fact]
        public void TestAutoSuggestFunction()
        {
            Environment.SetEnvironmentVariable("MaxItemsReturned", "10");
            // Invoke the lambda function and confirm the string was upper cased.
            var function = new FunctionAutoSuggest();
            var context = new TestLambdaContext();
            var algo = new APIGatewayProxyRequest()
            {
                //18000691 , 18000809, 4001261, 18000691,CE Alberca Cuadrangular
                PathParameters = new Dictionary<string, string>() { { "keyword", "cama" } },
                QueryStringParameters = new Dictionary<string, string>() { { "inv", "true" } },
                StageVariables = this.stageVariables
            };
            var upperCase = function.FunctionAutoSuggestHandler(algo, context);
            //Assert.Equal("HELLO WORLD", upperCase);
        }

        [Fact]
        public void TestPagedSearchFunction()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new FunctionPagedSearch();
            var context = new TestLambdaContext();
            var algo = new APIGatewayProxyRequest()
            {
                QueryStringParameters = new Dictionary<string, string>() { { "INV", "true" }, { "ft", "cama" }, { "_from", "0" }, { "_to", "49" } },
                //QueryStringParameters = new Dictionary<string, string>() { { "INV", "false" }, { "ft", "18000809" } },
                StageVariables = this.stageVariables
            };

            var upperCase = function.FunctionPagedSearchHandler(algo, context);
            //Assert.Equal("HELLO WORLD", upperCase);
        }


        [Fact]
        public void TestSkuDetailFunction()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new FunctionSkuDetail();
            var context = new TestLambdaContext();
            var algo = new APIGatewayProxyRequest()
            {
                //18000691 , 18000809, 4001261, 18000691,18000691,847965
                PathParameters = new Dictionary<string, string>() { { "sku", "18000905" } },
                StageVariables = this.stageVariables
            };
            var upperCase = function.FunctionSkuDetailHandler(algo, context);
            //Assert.Equal("HELLO WORLD", upperCase);
        }


        [Fact]
        public void TestPaymentComplement()
        {
            var function = new SetPayment();
            var context = new TestLambdaContext();
            string request = "{\"installments\": 1, \"BankName\": \"amex\", \"BankBin\": \"371773-1004\",  \"amount\": 500.00,";
            request += "\"authorizationId\": \"2324553\", \"paymentType\": 1, \"reference\": \"E-7123455\", \"paymentDate\": \"2019-02-01T00:00:00\"}";

            var algo = new APIGatewayProxyRequest()
            {
                StageVariables = this.stageVariables,
                Body = request
            };

            var response = function.FuncSetPayCompHan(algo, context);
        }


        [Fact]
        public void TestPaymentComplementCE()
        {
            var function = new function();
            var context = new TestLambdaContext();
            string request = "{\"info\":{\"estacionTrabajo\":\"SERVER\",\"idUsuario\":\"999990\",\"informacionCliente\":{\"ApellidoMaternoCliente\":\"\",\"ApellidoPaternoCliente\":\"Lozano\",\"Email\":\"eder.lozano@elektra.com.mx\",\"NombreCliente\":\"Eder\",\"NombreCompletoCliente\":\"Eder Lozano\",\"domicilio\":{\"calle\":\"Fernando Lizardi\",\"codigoPostal\":\"09260\",\"colonia\":\"Constituci\u00f3n de 1917\",\"estado\":\"CIUDAD DE M\u00c9XICO\",\"numeroExterno\":\"13\",\"numeroInterno\":\"\",\"poblacion\":\"Iztapalapa\"}},\"informacionVenta\":[{\"cantidad\":1,\"costoProducto\":0,\"descuento\":0,\"lstMilenias\":[{\"sku\":0,\"sobreprecio\":0}],\"lstPromociones\":[{\"cantidad\":1,\"idPromocion\":4939,\"idRegalo\":911,\"monto\":426.55,\"subTipoPromocion\":1,\"tipoPromocion\":32}],\"mecanica\":0,\"precio\":449,\"sku\":18000810,\"sobreprecio\":0,\"descuentoEspecial\":22.45}],\"referenciaEktCom\":\"521678\",\"tipoVenta\":1,\"totalVenta\":426.55,\"informacionPago\":{\"tipoPago\":201,\"plazoMsi\":1,\"bancoId\":\"911\",\"numeroTarjeta\":null,\"referenciaPago\":\"123456\",\"monto\":426.55},\"consecutivoPedido\":1,\"costoEstimado\":0,\"tipoEntrega\":1,\"proveedorEnvio\":\"ESTAFEPAQ\",\"omnicanalId\":1,\"totalPedidos\":2,\"afectaConta\":0,\"marketPlaceId\":0,\"sellerId\":0,\"montoVentaMP\":0,\"montoComision\":0,\"penalizacionId\":0,\"montoPenalizacion\":0,\"origenEnvioId\":1,\"pedidoOtorgaRegalo\":0,\"skuOtorgaRegalo\":0,\"tiendaSurt\":0},\"paymentcomplement\":{\"amount\":\"7499\",\"authorizationId\":\"91256698\",\"paymentType\":\"916\",\"reference\":\"E-91256698\",\"paymentDate\":\"2020-02-21T00:00:00\",\"employeeId\":\"8500335 \"}}";
            var algo = new APIGatewayProxyRequest()
            {
                StageVariables = this.stageVariables,
                Body = request
            };
            var response = function.SetPaymentCE(algo, context);
        }


        [Fact]
        public void TestCancelAE()
        {
            var function = new CancelAE();
            var context = new TestLambdaContext();
            string request = "{\"fcReference\": \"987987897897\"}";

            var algo = new APIGatewayProxyRequest()
            {
                StageVariables = this.stageVariables,
                Body = request
            };

            var response = function.FuncSetCancelAECompHan(algo, context);
        }
    }
}
