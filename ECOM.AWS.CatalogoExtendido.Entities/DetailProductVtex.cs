﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class DetailProductVtex
    {
        [JsonProperty("productId")]
        public long ProductId { get; set; }

        [JsonProperty("productName")]
        public string ProductName { get; set; }

        [JsonProperty("brand")]
        public string Brand { get; set; }

        [JsonProperty("brandId")]
        public long BrandId { get; set; }

        [JsonProperty("brandImageUrl")]
        public object BrandImageUrl { get; set; }

        [JsonProperty("linkText")]
        public string LinkText { get; set; }

        [JsonProperty("productReference")]
        public long ProductReference { get; set; }

        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }

        [JsonProperty("productTitle")]
        public string ProductTitle { get; set; }

        [JsonProperty("metaTagDescription")]
        public string MetaTagDescription { get; set; }

        [JsonProperty("releaseDate")]
        public DateTimeOffset? ReleaseDate { get; set; }

        [JsonProperty("clusterHighlights")]
        public ClusterHighlights ClusterHighlights { get; set; }

        [JsonProperty("productClusters")]
        public Dictionary<string, string> ProductClusters { get; set; }

        [JsonProperty("searchableClusters")]
        public Dictionary<string, string> SearchableClusters { get; set; }

        [JsonProperty("categories")]
        public List<string> Categories { get; set; }

        [JsonProperty("categoriesIds")]
        public List<string> CategoriesIds { get; set; }

        [JsonProperty("link")]
        public Uri Link { get; set; }

        [JsonProperty("Abonoprecios")]
        public List<string> Abonoprecios { get; set; }

        [JsonProperty("ForzarPrecio")]
        public List<string> ForzarPrecio { get; set; }

        [JsonProperty("Modelo")]
        public List<string> Modelo { get; set; }

        [JsonProperty("Dimensiones (L x Al x An)")]
        public List<string> DimensionesLXAlXAn { get; set; }

        [JsonProperty("Customer Bennefits 1")]
        public List<string> CustomerBennefits1 { get; set; }

        [JsonProperty("Customer Bennefits 2")]
        public List<string> CustomerBennefits2 { get; set; }

        [JsonProperty("Customer Bennefits 3")]
        public List<string> CustomerBennefits3 { get; set; }

        [JsonProperty("Customer Bennefits 4")]
        public List<string> CustomerBennefits4 { get; set; }

        [JsonProperty("Recomendaciones")]
        public List<string> Recomendaciones { get; set; }

        [JsonProperty("Contenido del Empaque")]
        public List<string> ContenidoDelEmpaque { get; set; }

        [JsonProperty("Garantía con Proveedor")]
        public List<string> GarantíaConProveedor { get; set; }

        [JsonProperty("Color")]
        public List<string> Color { get; set; }

        [JsonProperty("Material")]
        public List<string> Material { get; set; }

        [JsonProperty("Tela")]
        public List<string> Tela { get; set; }

        [JsonProperty("Estilo")]
        public List<string> Estilo { get; set; }

        [JsonProperty("Armable")]
        public List<string> Armable { get; set; }

        [JsonProperty("Atributos generales")]
        public List<string> AtributosGenerales { get; set; }

        [JsonProperty("allSpecifications")]
        public List<string> AllSpecifications { get; set; }

        [JsonProperty("allSpecificationsGroups")]
        public List<string> AllSpecificationsGroups { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("items")]
        public List<Item> Items { get; set; }
    }

    public class ClusterHighlights
    {
        [JsonProperty("392")]
        public string The392 { get; set; }
    }

    public class Item
    {
        [JsonProperty("itemId")]
        public long ItemId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("nameComplete")]
        public string NameComplete { get; set; }

        [JsonProperty("complementName")]
        public string ComplementName { get; set; }

        [JsonProperty("ean")]
        public long Ean { get; set; }

        [JsonProperty("referenceId")]
        public List<ReferenceId> ReferenceId { get; set; }

        [JsonProperty("measurementUnit")]
        public string MeasurementUnit { get; set; }

        [JsonProperty("unitMultiplier")]
        public long UnitMultiplier { get; set; }

        [JsonProperty("modalType")]
        public object ModalType { get; set; }

        [JsonProperty("isKit")]
        public bool IsKit { get; set; }

        [JsonProperty("images")]
        public List<Image> Images { get; set; }

        [JsonProperty("sellers")]
        public List<Seller> Sellers { get; set; }

        [JsonProperty("Videos")]
        public List<object> Videos { get; set; }

        [JsonProperty("estimatedDateArrival")]
        public object EstimatedDateArrival { get; set; }
    }

    public class Image
    {
        [JsonProperty("imageId")]
        public long ImageId { get; set; }

        [JsonProperty("imageLabel")]
        public string ImageLabel { get; set; }

        [JsonProperty("imageTag")]
        public string ImageTag { get; set; }

        [JsonProperty("imageUrl")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("imageText")]
        public string ImageText { get; set; }

        [JsonProperty("imageLastModified")]
        public DateTimeOffset? ImageLastModified { get; set; }
    }

    public class ReferenceId
    {
        [JsonProperty("Key")]
        public string Key { get; set; }

        [JsonProperty("Value")]
        public long Value { get; set; }
    }

    public class Seller
    {
        [JsonProperty("sellerId")]
        public long SellerId { get; set; }

        [JsonProperty("sellerName")]
        public string SellerName { get; set; }

        [JsonProperty("addToCartLink")]
        public Uri AddToCartLink { get; set; }

        [JsonProperty("sellerDefault")]
        public bool SellerDefault { get; set; }

        [JsonProperty("commertialOffer")]
        public CommertialOffer CommertialOffer { get; set; }
    }

    public class CommertialOffer
    {
        [JsonProperty("DeliverySlaSamplesPerRegion")]
        public DeliverySlaSamplesPerRegion DeliverySlaSamplesPerRegion { get; set; }

        [JsonProperty("Installments")]
        public List<Installment> Installments { get; set; }

        [JsonProperty("DiscountHighLight")]
        public List<object> DiscountHighLight { get; set; }

        [JsonProperty("GiftSkuIds")]
        public List<object> GiftSkuIds { get; set; }

        [JsonProperty("Teasers")]
        public List<object> Teasers { get; set; }

        [JsonProperty("BuyTogether")]
        public List<object> BuyTogether { get; set; }

        [JsonProperty("ItemMetadataAttachment")]
        public List<object> ItemMetadataAttachment { get; set; }

        [JsonProperty("Price")]
        public long Price { get; set; }

        [JsonProperty("ListPrice")]
        public long ListPrice { get; set; }

        [JsonProperty("PriceWithoutDiscount")]
        public long PriceWithoutDiscount { get; set; }

        [JsonProperty("RewardValue")]
        public long RewardValue { get; set; }

        [JsonProperty("PriceValidUntil")]
        public DateTimeOffset? PriceValidUntil { get; set; }

        [JsonProperty("AvailableQuantity")]
        public long AvailableQuantity { get; set; }

        [JsonProperty("Tax")]
        public long Tax { get; set; }

        [JsonProperty("DeliverySlaSamples")]
        public List<DeliverySlaSample> DeliverySlaSamples { get; set; }

        [JsonProperty("GetInfoErrorMessage")]
        public object GetInfoErrorMessage { get; set; }

        [JsonProperty("CacheVersionUsedToCallCheckout")]
        public string CacheVersionUsedToCallCheckout { get; set; }
    }

    public class DeliverySlaSample
    {
        [JsonProperty("DeliverySlaPerTypes")]
        public List<object> DeliverySlaPerTypes { get; set; }

        [JsonProperty("Region")]
        public object Region { get; set; }
    }

    public class DeliverySlaSamplesPerRegion
    {
        [JsonProperty("0")]
        public DeliverySlaSample The0 { get; set; }
    }

    public class Installment
    {
        [JsonProperty("Value")]
        public double Value { get; set; }

        [JsonProperty("InterestRate")]
        public long InterestRate { get; set; }

        [JsonProperty("TotalValuePlusInterestRate")]
        public long TotalValuePlusInterestRate { get; set; }

        [JsonProperty("NumberOfInstallments")]
        public long NumberOfInstallments { get; set; }

        [JsonProperty("PaymentSystemName")]
        public string PaymentSystemName { get; set; }

        [JsonProperty("PaymentSystemGroupName")]
        public string PaymentSystemGroupName { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }
    }

}
