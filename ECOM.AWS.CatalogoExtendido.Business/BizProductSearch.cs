﻿using System;
using System.Collections.Generic;
using ECOM.AWS.CatalogoExtendido.Entities;
using ECOM.AWS.CatalogoExtendido.Services;
using Newtonsoft.Json;
using System.Net;
using System.Linq;
using AWS.CatalogoExtendido.Core;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Amazon.Lambda.Core;

namespace ECOM.AWS.CatalogoExtendido.Business
{
    public class BizProductSearch
    {
        #region Members
        const string kElektraSellerId = "1";
        const string kAvoidCatKey = "avoidcategories";
        #endregion

        #region Webservice
        WSProduct wsProd = new WSProduct();

        #endregion

        #region Public methods
        public C3MethodResponse<List<AutoSuggestResponse>> AutoSuggestProduct(Dictionary<string, string> pathParameters, Dictionary<string, string> queryParameters)
        {
            var loggerLambda = new LoggerLambda();
            loggerLambda.ToWriterLoggs("START-AutoSuggestProduct", StaticVariables.FlgLogger);
            var response = new C3MethodResponse<List<AutoSuggestResponse>>();
            const string kKeyWord = "keyword";
            const string kKeyInventory = "inv";
            const string kElektraSellerId = "1";
            bool onlyItemsWithInventory = false;
            var asrList = new List<AutoSuggestResponse>();
            var inventory = queryParameters[kKeyInventory];
            string keyword = pathParameters[kKeyWord].Trim();
            var wsProd = new WSProduct();

            if (pathParameters == null || (pathParameters != null && !pathParameters.ContainsKey(kKeyWord)))
            {
                throw new ArgumentException("No se específico un parámetro para la búsqueda.");
            }
            try
            {

                if (queryParameters != null && queryParameters.ContainsKey(kKeyInventory))
                {
                    onlyItemsWithInventory = Convert.ToBoolean(queryParameters[kKeyInventory]);
                }
                var resulAutoSuggest = wsProd.AutoSuggestSearchBy(keyword, null, inventory);
                foreach (var product in resulAutoSuggest)
                {
                    var pr = new AutoSuggestResponse();
                    var infoItem = (from prItem in product.Items
                                    where prItem.ItemId == product.ProductId.ToString()
                                    select prItem).FirstOrDefault();
                    pr.productId = product.ProductId;
                    pr.productName = product.ProductName;
                    pr.inventory = (
                                    from sll in infoItem.Sellers
                                    where sll.SellerId == kElektraSellerId
                                    select sll).FirstOrDefault().CommertialOffer.AvailableQuantity;
                    if (infoItem.Images != null && infoItem.Images.Count > 0)
                    {
                        var img = infoItem.Images.Where(im => !im.imageText.Contains("-"));
                        string img2 = null;
                        if (img.Count() == 0)
                        {
                            img2 = infoItem.Images[0].imageUrl;
                        }
                        else
                        {
                            img2 = img.FirstOrDefault().imageUrl;
                        }
                        pr.imageUrl = img2;

                        asrList.Add(pr);
                    }
                }
                response.code = (int)HttpStatusCode.OK;
                response.message = null;
                response.more_info = null;
                response.result = asrList;
            }
            catch (Exception ex)
            {
                response.code = (int)HttpStatusCode.InternalServerError;
                response.message = "Ocurrió un error en la búsqueda por palabra.";
                response.more_info = JsonConvert.SerializeObject(ex);
                loggerLambda.ToWriterLoggs("Exception-AutoSuggestProduct" + JsonConvert.SerializeObject(response), StaticVariables.FlgLogger);

            }
            loggerLambda.ToWriterLoggs("END-AutoSuggestProduct", StaticVariables.FlgLogger);
            return response;
        }
        public C3MethodResponse<List<PagedSearchResponse>> PagedSearch(Dictionary<string, string> queryParameters, out string resources)
        {
            const string kKeyInventory = "inv";
            string searchResources = string.Empty;
            bool onlyItemsWithInventory = false;
            string totalElementesAfterFilter = string.Empty;
            var response = new C3MethodResponse<List<PagedSearchResponse>>();
            var loggerLambda = new LoggerLambda();


            try
            {
                if (queryParameters != null && queryParameters.ContainsKey(kKeyInventory))
                {
                    onlyItemsWithInventory = Convert.ToBoolean(queryParameters[kKeyInventory]);
                    queryParameters.Remove(kKeyInventory);
                }

                var paginSearchWs = wsProd.SearchWithPaging(queryParameters, out searchResources);
                var results = JsonConvert.DeserializeObject<List<VtexProductsSearchResponse>>(paginSearchWs.Content);
                var filInvProd = FilterItemsWithInventory(results, onlyItemsWithInventory);

                List<PagedSearchResponse> listProduct = filInvProd.Select(fr => new PagedSearchResponse
                {
                    //AbonoPrecios = fr.AbonoPrecios,//{\"weeks\":78,\"weekly\":61}                    
                    AbonoPrecios = formatAbonoPrecio(fr.AbonoPrecios),
                    Brand = fr.Brand,
                    Description = fr.Description,
                    Modelo = fr.Modelo,
                    ProductId = fr.ProductId,
                    ProductName = fr.ProductName
                }).ToList();

                foreach (var prod in listProduct)
                {
                    {
                        var itm = (from fr in filInvProd
                                   where fr.ProductId == prod.ProductId
                                   from item in fr.Items
                                   where item.ItemId.Equals(prod.ProductId.ToString())
                                   select item
                                       ).FirstOrDefault();

                        var commertialInfo = (from sll in itm.Sellers
                                              where sll.SellerId.Equals(kElektraSellerId)
                                              select sll).FirstOrDefault().CommertialOffer;
                        prod.ListPrice = commertialInfo.ListPrice;
                        prod.Price = commertialInfo.Price;
                        prod.PriceWithoutDiscount = commertialInfo.PriceWithoutDiscount;
                        prod.AvailableQuantity = wsProd.ProductInventory(Convert.ToString(prod.ProductId));
                        if (itm.Images != null && itm.Images.Count > 0)
                        {
                            var img = itm.Images.Where(
                                im => !im.imageText.Contains("-")
                                );
                            string img2 = null;
                            if (img.Count() == 0)
                            {
                                img2 = itm.Images[0].imageUrl;
                            }
                            else
                            {
                                img2 = img.FirstOrDefault().imageUrl;
                            }
                            prod.ImageUrl = img2;
                        }
                        var abono = prod.AbonoPrecios?[0];
                        prod.CreditInfo = BizProductDetail.CalculateCreditInfo(prod.Price, abono);
                    }

                    response.code = (int)HttpStatusCode.PartialContent;
                    response.message = null;
                    response.more_info = null;
                    response.result = listProduct;
                    totalElementesAfterFilter = "/" + listProduct.Count.ToString();
                }
            }
            catch (Exception ex)
            {
                loggerLambda.ToWriterLoggs("EXCEPTION-LOG-PagedSearch ||" + JsonConvert.SerializeObject(ex), StaticVariables.FlgLogger);
                response.code = (int)HttpStatusCode.InternalServerError;
                response.message = "Ocurrió un error en la búsqueda de producto con paginación.";
            }
            resources = searchResources + totalElementesAfterFilter;
            return response;
        }

        //Formatear el json de Abonos para devolver 0.5340|17.90|128
        private List<string> formatAbonoPrecio(List<string> abonoPrecios)
        {
            string formatAbono = string.Empty;
            List<string> listAbonos = null;
            if (abonoPrecios != null)
            {
                listAbonos = new List<string>();
                foreach (var itemAbono in abonoPrecios)
                {
                    if (itemAbono.Contains("{"))
                    {
                        var week = JObject.Parse(itemAbono);
                        if (week.GetValue("weeks") != null)
                        {
                            if (!string.IsNullOrWhiteSpace(itemAbono))
                            {
                                var abonojson = JObject.Parse(itemAbono);
                                formatAbono = "0.5340|17.90|" + week.GetValue("weeks");//"0.5340|17.90|"valor taza y enganche parseado temporalmente, se toma unicamente semana
                            }
                        }
                        else
                        {
                            return listAbonos = null;
                        }
                    }
                    else if (itemAbono.Contains('|'))
                    {
                        formatAbono = itemAbono;
                    }
                    listAbonos.Add(formatAbono);
                }
            }
            return listAbonos;
        }
        #endregion

        #region Private Methods
        private List<VtexProductsSearchResponse> FilterItemsWithInventory(List<VtexProductsSearchResponse> vtexItems, bool onlyItemsWithInventory)
        {
            var filteredItems = new List<VtexProductsSearchResponse>();
            var listConcurrent = new List<VtexProductsSearchResponse>();
            var invalidCategories = new List<string>();
            WSProduct wsInventory = new WSProduct();

            foreach (var kvp in StaticVariables.Variables)
            {
                if (kvp.Key.ToLower().Contains(kAvoidCatKey))
                {
                    byte[] dataCat = System.Convert.FromBase64String(kvp.Value);
                    var cat = System.Text.Encoding.UTF8.GetString(dataCat);
                    invalidCategories.AddRange(cat.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList<string>());
                }
            }

            vtexItems = vtexItems.Where(p => !invalidCategories.Contains(p.CategoryId)).ToList();


            Parallel.ForEach(vtexItems, prod =>
            {
                prod.Items[0].Sellers[0].CommertialOffer.AvailableQuantity = wsInventory.ProductInventory(Convert.ToString(prod.ProductId));
                listConcurrent.Add(prod);
            });

            if (listConcurrent.Count > 0)
            {
                if (onlyItemsWithInventory)
                {
                    filteredItems = (from vitm in vtexItems
                                     from itm in vitm.Items
                                     where itm.ItemId == vitm.ProductId.ToString()
                                     from sll in itm.Sellers
                                     where sll.SellerId == kElektraSellerId && sll.CommertialOffer.AvailableQuantity > 0
                                     select vitm).ToList();
                }
                else
                {
                    filteredItems = (from vitm in vtexItems
                                     from itm in vitm.Items
                                     where itm.ItemId == vitm.ProductId.ToString()
                                         && itm.Sellers.Count == 1 && itm.Sellers[0].SellerId == kElektraSellerId
                                     select vitm).ToList();
                }
            }

            return filteredItems;
        }
        #endregion
    }
}
