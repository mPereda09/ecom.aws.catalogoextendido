using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using AWS.CatalogoExtendido.Core;
using ECOM.AWS.CatalogoExtendido.Business;
using ECOM.AWS.CatalogoExtendido.Entities;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AWS.CatalogoExtendido.Main.DetailSkus
{
    public class FunctionDetailSkus
    {

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public APIGatewayProxyResponse GetListSkuDetail(APIGatewayProxyRequest request, ILambdaContext context)
        {
            ProductDetailResponse BzResponse = new ProductDetailResponse();

            try
            {
                Dictionary<string, string> queryParams = request.QueryStringParameters != null ? new Dictionary<string, string>(request.QueryStringParameters, StringComparer.OrdinalIgnoreCase) : null;
                Dictionary<string, string> stageVariables = request.StageVariables != null ? new Dictionary<string, string>(request.StageVariables, StringComparer.OrdinalIgnoreCase) : null;
                StaticVariables.SetStageVariables(stageVariables);
                BzProductsDetailAll bzProduct = new BzProductsDetailAll();
                BzResponse = bzProduct.GetProductsDetail(request.Body);
            }
            catch (Exception ex)
            {
                return new APIGatewayProxyResponse()
                {
                    Body = JsonConvert.SerializeObject(ex.Message),
                    IsBase64Encoded = false,
                    StatusCode = (int)HttpStatusCode.InternalServerError
                };
            }

            return new APIGatewayProxyResponse()
            {
                Body = JsonConvert.SerializeObject(BzResponse),
                IsBase64Encoded = false,
                StatusCode = (int)BzResponse.Code
            };
        }
    }
}
