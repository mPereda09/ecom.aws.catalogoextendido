﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECOM.AWS.CatalogoExtendido.Entities
{

    public class ResponsePayment
    {
        [JsonProperty("prodId")]
        public int ProdId { get; set; }

        [JsonProperty("noVenta")]
        public int NoVenta { get; set; }
    }

}
