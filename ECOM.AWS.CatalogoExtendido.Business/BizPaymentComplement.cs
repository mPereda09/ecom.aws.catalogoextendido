﻿using System;
using System.Collections.Generic;
using System.Text;
using ECOM.AWS.CatalogoExtendido.Services;
using Newtonsoft.Json;
using System.Dynamic;
using Amazon.Lambda.Core;
using AWS.CatalogoExtendido.Core;

namespace ECOM.AWS.CatalogoExtendido.Business
{
    public class BizPaymentComplement
    {
        public static object SetPaymentComplement(object info)
        {
            try
            {
                var response = WSPaymentComplement.SetPaymentComplement(info);
                return response;
            }
            catch (Exception ex)
            {
                dynamic error = new ExpandoObject();

                error.Code = 636873088449233257;
                error.Message = "Ocurrio un error al ejecutar el complemento de pago.";
                error.Result = null;

                string stringLog = "{\"Code\":636873088449233257,\"Message\":\"" + "Ocurrio un error al ejecutar el complemento de pago." + " More Info :" + ex.Message.ToString() + " DetailInfo :" + ex.StackTrace.ToString() + "\",\"Result\":null}";
                LambdaLogger.Log("LogErrorException |" + "| INFO |" + "RESPONSE :" + stringLog);

                return JsonConvert.SerializeObject(error);
            }
        }
    }
}
