﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ECOM.AWS.CatalogoExtendido.Entities
{
    public class ProductCreditInfo
    {
        public decimal PagoPuntual { get; set; }
        public int PlazoMaximo { get; set; }
    }
    public class Dimension
    {
        [JsonProperty(PropertyName = "cubicweight")]
        public float cubicweight { get; set; }

        [JsonProperty(PropertyName = "height")]
        public float height { get; set; }

        [JsonProperty(PropertyName = "length")]
        public float length { get; set; }

        [JsonProperty(PropertyName = "weight")]
        public float weight { get; set; }

        [JsonProperty(PropertyName = "width")]
        public float width { get; set; }
    }

    public class RealDimension
    {
        [JsonProperty(PropertyName = "realCubicWeight")]
        public float realCubicWeight { get; set; }

        [JsonProperty(PropertyName = "realHeight")]
        public float realHeight { get; set; }

        [JsonProperty(PropertyName = "realLength")]
        public float realLength { get; set; }

        [JsonProperty(PropertyName = "realWeight")]
        public float realWeight { get; set; }

        [JsonProperty(PropertyName = "realWidth")]
        public float realWidth { get; set; }
    }

    public class ImageProductSku
    {
        [JsonProperty(PropertyName = "ImageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "ImageName")]
        public string ImageName { get; set; }

        [JsonProperty(PropertyName = "FileId")]
        public int FileId { get; set; }
    }

    public class ProductSpecification
    {
        [JsonProperty(PropertyName = "FieldId")]
        public int FieldId { get; set; }

        [JsonProperty(PropertyName = "FieldName")]
        public string FieldName { get; set; }

        [JsonProperty(PropertyName = "FieldValueIds")]
        public List<string> FieldValueIds { get; set; }

        [JsonProperty(PropertyName = "FieldValues")]
        public List<string> FieldValues { get; set; }
    }

    public class ProductDetail
    {
        [JsonProperty(PropertyName = "ProductId")]
        public string ProductId { get; set; }

        [JsonProperty(PropertyName = "NameComplete")]
        public string NameComplete { get; set; }

        [JsonProperty(PropertyName = "ProductName")]
        public string ProductName { get; set; }

        [JsonProperty(PropertyName = "ProductDescription")]
        public string ProductDescription { get; set; }

        [JsonProperty(PropertyName = "ImageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "BrandName")]
        public string BrandName { get; set; }

        [JsonProperty(PropertyName = "Dimension")]
        public Dimension Dimension { get; set; }

        [JsonProperty(PropertyName = "RealDimension")]
        public RealDimension RealDimension { get; set; }

        [JsonProperty(PropertyName = "Images")]
        public List<ImageProductSku> Images { get; set; }

        [JsonProperty(PropertyName = "ProductSpecifications")]
        public List<ProductSpecification> ProductSpecifications { get; set; }

        [JsonProperty(PropertyName = "ProductCategories")]
        public Dictionary<string, string> ProductCategories { get; set; }
        public decimal Price { get; set; }
        public decimal ListPrice { get; set; }
        public int AvailableQuantity { get; set; }
        public string EstimatedDeliveryTime { get; set; }
        public ProductCreditInfo CreditInfo { get; set; }

        public ProductDetail()
        {
            this.CreditInfo = new ProductCreditInfo();
            this.EstimatedDeliveryTime = "Tiempo estimado de entrega de 2 a 10 días hábiles.";
        }
    }
}
